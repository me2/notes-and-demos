; (function (w, d, undefined) {

    const html = d.querySelector('html')

    d.addEventListener('DOMContentLoaded', function () {

        // burger menu

        const menuIcon = d.querySelector('.menu-icon')

        if (menuIcon) {
            menuIcon.addEventListener('click', () => {
                menuIcon.classList.toggle('menu-active')
                d.body.classList.toggle('menu-active')
            })
        }

        // progress

        const scrollProgress = function () {
            d.body.style.setProperty('--docHeightScrollPercentage', Math.floor(w.pageYOffset / (d.body.offsetHeight - w.innerHeight) * 100) + "%")
        }

        window.addEventListener('scroll', scrollProgress, false)
        scrollProgress()

        // back to top

        const btt = d.querySelector('a[href="#top"]');

        if (btt) {
            btt.addEventListener("click", event => {
                event.preventDefault();
                const target = d.querySelector(event.target.hash)
                target.scrollIntoView({
                    behavior: "smooth",
                    block: "start"
                })
            })
        }

        // share w/ native support

        const shareButton = d.querySelector('.share-button')

        if (shareButton) {

            const shareDialog = d.querySelector('.share-dialog')
            const shareCloseButton = d.querySelector('.share-close-button')

            shareButton.addEventListener('click', event => {
                const pageTitle = d.title
                const pageUrl = d.location.href

                if (navigator.share) {
                    navigator.share({
                        title: pageTitle,
                        url: pageUrl
                    }).then(() => {
                        console.log('Thanks for sharing!')
                    }).catch(console.error)
                } else {
                    shareDialog.classList.add('is-open')

                    const targets = shareDialog.querySelector('[data-targets-unprocessed]')

                    if (targets) {
                        targets.removeAttribute('data-targets-unprocessed')

                        const shareLinks = targets.querySelectorAll('a')
                        shareLinks.forEach((shareLink) => {
                            shareLink.setAttribute('href', shareLink.getAttribute('href') + pageUrl)
                        })

                        const copyShareUrl = shareDialog.querySelector('.share-url')
                        copyShareUrl.innerText = pageUrl
                    }
                }
            })

            shareCloseButton.addEventListener('click', event => {
                shareDialog.classList.remove('is-open')
            })
        }
    })

    // algolia search & instantsearch

    const monthNames = [ "Jan", "Febr", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]

    const search = instantsearch(window.site.algolia)

    const hitTemplate = function (hit) {

        if (!hit._highlightResult) {
            return
        }

        let date = ''
        let rawDate = ''
        if (hit.date) {
            rawDate = hit.date
            date = new Date((hit.date + 5 * 60 * 60) * 1000)
            date = `${monthNames[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`
        }
        const url = hit.url
        const title = hit._highlightResult.title.value

        return `
        <div class="${hit.type}-type li">
            <span class="post-meta"><time datetime="${rawDate}">${date}</time></span>
            <h2><a class="post-link" href="${window.site.baseurl}${url}">${title}</a></h2>
        </div>
        `
    }

    search.addWidget(
        instantsearch.widgets.searchBox({
            container: '#search-form',
            poweredBy: false,
            showReset: false,
            placeholder: 'Search'
        })
    )

    search.addWidget(
        instantsearch.widgets.hits({
            container: '#search-hits',
            templates: {
                item: hitTemplate
            }
        })
    )

    search.start()

    const searchInput = d.querySelector('.ais-search-box--input')
    const searchResults = d.querySelector('#search-results')

    setTimeout(function () {
        searchInput.blur()
    }, 100)

    searchInput.setAttribute('aria-label', 'enter a search term, and click enter')

    searchInput.addEventListener('focus', function () {
        searchResults.style.opacity = '1'
        searchResults.style.pointerEvents = 'all'
        html.setAttribute('data-search-open', true)
    })

    searchInput.addEventListener('blur', function () {
        searchResults.style.opacity = '0'
        searchResults.style.pointerEvents = 'none'
        html.removeAttribute('data-search-open')
    })

    // just for 404 search link suggestion
    const searchFocusLink = d.querySelector('a[href="#404-search-focus"]')
    if (searchFocusLink) {
        searchFocusLink.addEventListener('click', function (e) {
            e.preventDefault()
            searchInput.focus()
        })
    }

}) (window, document)
