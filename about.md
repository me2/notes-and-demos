---
layout: page
title: About
permalink: /about/
---

## this site

It's [jekyll](https://jekyllrb.com), a static site generator hosted by [gitlab.com](https://gitlab.com), here: <{{site.baseurl | prepend: site.url}}> ... Jekyll is probably the most popular static site generator and because of that has a lot of traction in the open source community.  There are lots of plugins & themes (see below); and lots of resources at your disposal to solve problems.

Steps to set this up:

* Fork this project: <https://gitlab.com/pages/jekyll>
* Follow the 5 steps here: <https://gitlab.com/pages/jekyll#start-by-forking-this-repository>

You should now have a Jekyll site here: your-repo-name.gitlab.io/some-base-url to visit after commiting at least one change & triggering the Pipeline needed to build and deploy your static site.

Also note: Jekyll uses [Liquid](https://shopify.github.io/liquid/).

## my workflow

### for publishing...

Use [Vim](https://www.vim.org/) & git from the from the command line to be [blogging like a hacker](http://tom.preston-werner.com/2008/11/17/blogging-like-a-hacker.html).

The `git push` command triggers the Gitlab Pipeline.

It's really that easy.

Less like a hacker? Try:

VS Code & this extension to start new posts: [jekyll-post](https://marketplace.visualstudio.com/items?itemName=rohgarg.jekyll-post).  After creating a post, and the Git UI within VS Code to stage, commit & push posts ... or:

### if i'm updating the theme or adding new functionality...

Run a local server (e.g. localhost:4000/notes-and-demos/), with this command:

`bundle exec jekyll serve`

In a separate Shell, if you're working on updates to CSS, run this command to watch Javascript & Less files:

`gulp`

On localhost:3000 -- without refreshing -- you'll see Posts updated, as well as CSS & JS.

## there are tons of themes & plugins

## go to: <https://jekyllrb.com/resources/>

Or, checkout: <https://rubygems.org/search?utf8=%E2%9C%93&query=jekyll>

I might add these:

* <https://github.com/allenlsy/jekyll-galleries>

## installed plugins:

### Algolia (for Site Search)

I've already added [Algolia](https://www.algolia.com/).

It's easy to set up, except -- at least for me -- this part: you'll have two API keys to work with from Algolia.

* One is private called `ALGOLIA_API_KEY`
* One is public called `search_only_api_key`

The public key (`search_only_api_key`) & `application_id` along with an `index_name` all live in `_config.yml` and are in code in this repository; which is fine.

The private key is referenced in this file `.gitlab-ci.yml` (right now on line 25):

`ALGOLIA_API_KEY="$ALGOLIA_API_KEY" bundle exec jekyll algolia`

Where `$ALGOLIA_API_KEY` is a variable passed from a Variable set here:

Go to https://gitlab.com/your-username/your-repo-name/-/settings/ci_cd ...

And use the section called Variabes to set a Variable & the value provided by Algolia.

(Note: that command that uses `$ALGOLIA_API_KEY` is part of the build & deploy process and sends new posts to Algolia for indexing.  So add a post, `git push` & behind the scenes Gitlab's Docker image associated with your repo's Pipeline kicks off indexing.)

This is where the UI for search is from: <https://www.algolia.com/doc/guides/building-search-ui/getting-started/js/>

#### This is a critical step in obscuring private keys needed for your jekyll site, if applicable ####

### content blocks

<https://github.com/rustygeldmacher/jekyll-contentblocks>

### sitemap xml (for SEO)

<https://github.com/jekyll/jekyll-sitemap>

### target blank

For an easy to install plugin to start with & to solve a simple problem: automate adding attributes on anchor tags to open new windows or tabs for external links:

<https://github.com/keithmifsud/jekyll-target-blank>

### jekyll webp

Add support for the webp image format:

<https://github.com/sverrirs/jekyll-webp>

### jekyll responsive images

Used in conjunction with the webp plugin above, add support for dynamically resizing images at build time to reduce delivering,
so that image sizes appropriately match device constraints.

<https://github.com/wildlyinaccurate/jekyll-responsive-image>

Locally, you might need to reinstall ImageMagick, to resolve compatability issues:

    brew install imagemagick
    PKG_CONFIG_PATH=/usr/local/Cellar/imagemagick@6/6.9.10-66/lib/pkgconfig/ gem install rmagick
    brew link --overwrite --force imagemagick@6
    bundle install

