const autoprefixer = require('autoprefixer')
const babel = require('gulp-babel')
const browserSync = require('browser-sync')
const concat = require('gulp-concat')
const cp = require('child_process')
const cssnano = require('cssnano')
const gulp = require('gulp')
const less = require('gulp-less')
const postcss = require('gulp-postcss')
const rename = require('gulp-rename')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')
const watch = require('gulp-watch')

const messages = {
    jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'
}

// Build the Jekyll Site
gulp.task('jekyll-build', (done) => {
    browserSync.notify(messages.jekyllBuild)
    return cp.spawn('jekyll', ['build'], { stdio: 'inherit' }).on('close', done)
})

// Rebuild Jekyll & do page reload when watched files change
gulp.task('jekyll-rebuild', ['jekyll-build'], () => {
    browserSync.reload()
})

// Wait for jekyll-build, then launch the Server
gulp.task('serve', ['jekyll-build'], () => {
    browserSync.init({
        server: "_site/",
        rewriteRules: [
            {
                match: /notes-and-demos\//g,
                fn: () => {
                    return ''
                }
            }
        ]
    })
})

// Watch jekyll source files for changes, don't watch assets
gulp.task('watch', () => {
    gulp.watch(['**/*.*', '!_site/**/*', '!less/*', '!js/*', '!node_modules/**/*'], ['jekyll-rebuild'])
})

// Watch just the less files - no need to rebuild jekyll
gulp.task('watch-less', ['less-rebuild'], () => {
    gulp.watch(['less/*.less'], ['less-rebuild'])
})

// Watch just the js files
gulp.task('watch-js', ['js-rebuild'], () => {
    gulp.watch(['js/*.js'], ['js-rebuild'])
})

// If less files change ...
gulp.task('less-rebuild', () => {
    var plugins = [
        autoprefixer({ browsers: ['last 2 version'] }),
        cssnano()
    ]

    return gulp.src('less/main.less')
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('_site/css/'))
        .pipe(gulp.dest('css/'))
        .pipe(browserSync.reload({
            stream: true
        }))
})

// If js files change ...

gulp.task('js-rebuild', ['js-compile'], function () {
    return gulp.src([
        './node_modules/lazysizes/lazysizes.min.js',
        './node_modules/instantsearch.js/dist/instantsearch.min.js',
        './js/main.compiled.js'])
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest('./js/'));
})

gulp.task('js-compile', () => {
    return gulp.src('js/main.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(rename({ extname: '.compiled.js' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('_site/js'))
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({
            stream: true
        }))
})

// default gulp task:
gulp.task('default', ['serve', 'watch', 'watch-less', 'watch-js'])
