---
layout: post
title:  example with codepen
published: true
---
<p class="codepen" data-height="300" data-theme-id="37078" data-default-tab="html,result" data-user="paulgpetty" data-slug-hash="RwbKOoo" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="RwbKOoo">
  <span>See the Pen <a href="https://codepen.io/paulgpetty/pen/RwbKOoo/">
  RwbKOoo</a> by Paul Griffin Petty (<a href="https://codepen.io/paulgpetty">@paulgpetty</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>
