---
layout: post
title: example with footnotes
published: true
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis sit amet sem id porttitor. Sed tempor elit a massa sagittis interdum. Ut imperdiet tellus ex, id sollicitudin metus tempus lobortis. Sed at est hendrerit, dignissim est vitae, dapibus lorem. Maecenas tincidunt, lorem sit amet luctus finibus, mi nulla auctor mauris, non ornare eros urna in urna. Aliquam sodales dui ligula, nec feugiat turpis ultrices vitae. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed tellus massa, pharetra vel tincidunt id, commodo id libero. Nulla diam augue, semper in fringilla cursus, maximus ut neque. Mauris semper efficitur elit nec finibus. Sed sodales eros tellus, non imperdiet leo lobortis at. Proin luctus ultrices orci, quis eleifend neque gravida ac. Fusce eget ipsum purus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed quam odio, vestibulum et leo ut, feugiat imperdiet augue. Donec tempus velit et auctor volutpat.

Pellentesque et ornare ante. Sed interdum blandit ipsum, eget suscipit purus congue vel. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer cursus in nibh ut pretium. Curabitur eu tortor finibus, suscipit orci auctor, commodo magna. Aenean facilisis tellus odio, nec viverra magna aliquam ac. Donec dignissim eros et efficitur sollicitudin.

Vivamus aliquet lacus eu scelerisque blandit. Sed at odio tincidunt, varius erat a, pharetra lectus. Aenean finibus leo in sem laoreet efficitur. Nullam non augue non dui ultricies imperdiet. Aenean at lobortis nulla. Phasellus iaculis semper turpis sit amet placerat. Maecenas faucibus risus ac viverra elementum. Morbi lobortis, nibh tincidunt vestibulum varius, sapien odio semper lorem, eu varius odio nibh eu dui. Aenean volutpat sed diam ac iaculis. Morbi vulputate, mi at malesuada rutrum, purus tellus semper ligula, vel elementum nulla augue ullamcorper elit.

Suspendisse fringilla orci in ipsum ullamcorper facilisis. Aenean dui erat, vehicula eu elit eget, vehicula iaculis urna. Nunc sed lorem ut ipsum maximus pretium sit amet vitae nisl. Nam luctus sapien quis elit laoreet convallis. Maecenas tincidunt eros at venenatis posuere. Donec malesuada erat nec fringilla condimentum. Ut luctus lectus leo, pulvinar aliquet elit accumsan eu. Etiam at ligula sit amet nulla viverra euismod a vel erat. Sed in consectetur nisi. Nam accumsan, lorem vitae eleifend tristique, ligula massa condimentum dolor, quis accumsan turpis urna sit amet turpis. Donec in metus eget enim consectetur egestas vel sit amet nisi. Maecenas diam lorem, condimentum id urna et, finibus varius tellus. Maecenas congue orci nec eros convallis rhoncus.



Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus facilisis sit amet sem id porttitor. Sed tempor elit a massa sagittis interdum. Ut imperdiet tellus ex, id sollicitudin metus tempus lobortis. Sed at est hendrerit, dignissim est vitae, dapibus lorem. Maecenas tincidunt, lorem sit amet luctus finibus, mi nulla auctor mauris, non ornare eros urna in urna. Aliquam sodales dui ligula, nec feugiat turpis ultrices vitae. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed tellus massa, pharetra vel tincidunt id, commodo id libero. Nulla diam augue, semper in fringilla cursus, maximus ut neque. Mauris semper efficitur elit nec finibus. Sed sodales eros tellus, non imperdiet leo lobortis at. Proin luctus ultrices orci, quis eleifend neque gravida ac. Fusce eget ipsum purus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed quam odio, vestibulum et leo ut, feugiat imperdiet augue. Donec tempus velit et auctor volutpat.



The quick brown fox[^1] jumped over the lazy dog[^2]. Pellentesque et ornare ante. Sed interdum blandit ipsum, eget suscipit purus congue vel. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer cursus in nibh ut pretium. Curabitur eu tortor finibus, suscipit orci auctor, commodo magna. Aenean facilisis tellus odio, nec viverra magna aliquam ac. Donec dignissim eros et efficitur sollicitudin.

Vivamus aliquet lacus eu scelerisque blandit. Sed at odio tincidunt, varius erat a, pharetra lectus. Aenean finibus leo in sem laoreet efficitur. Nullam non augue non dui ultricies imperdiet. Aenean at lobortis nulla. Phasellus iaculis semper turpis sit amet placerat. Maecenas faucibus risus ac viverra elementum. Morbi lobortis, nibh tincidunt vestibulum varius, sapien odio semper lorem, eu varius odio nibh eu dui. Aenean volutpat sed diam ac iaculis. Morbi vulputate, mi at malesuada rutrum, purus tellus semper ligula, vel elementum nulla augue ullamcorper elit.



The quick brown fox[^3] jumped over the lazy dog[^4]. Suspendisse fringilla orci in ipsum ullamcorper facilisis. Aenean dui erat, vehicula eu elit eget, vehicula iaculis urna. Nunc sed lorem ut ipsum maximus pretium sit amet vitae nisl. Nam luctus sapien quis elit laoreet convallis. Maecenas tincidunt eros at venenatis posuere. Donec malesuada erat nec fringilla condimentum. Ut luctus lectus leo, pulvinar aliquet elit accumsan eu. Etiam at ligula sit amet nulla viverra euismod a vel erat. Sed in consectetur nisi. Nam accumsan, lorem vitae eleifend tristique, ligula massa condimentum dolor, quis accumsan turpis urna sit amet turpis. Donec in metus eget enim consectetur egestas vel sit amet nisi. Maecenas diam lorem, condimentum id urna et, finibus varius tellus. Maecenas congue orci nec eros convallis rhoncus.

[^1]: Foxes are red
[^2]: Dogs are usually not red
[^3]: Foxes are rabid
[^4]: Dogs are nice

{% contentfor aside %}

# ok

{% endcontentfor %}
